<?php

class Controleur {

	function __construct() {
		require_once("config/config.php");
		require_once("modeles/Connection.php");
		require_once("modeles/gateWayNews.php");
		//global $rep,$vues,$dsn,$login,$pass;
		global $rep;
		// nécessaire pour utiliser variables globales
		// on démarre ou reprend la session
		session_start();
	
	
		//debut
	
		//on initialise un tableau d'erreur
		$dVueEreur = array ();
	
		//connection BBD
		
		try{
			$con=new Connection($dsn,$login,$pass);
		}
		catch( PDOException $Exception ) {
			$dVueEreur[] = "Connection impossible à la BBD !!";
			require($rep.$vues['erreur']);
		}
		$gateWayNews = new gateWayNews($con);
		
		//VALIDER L'ACTION 
	
		try{
			if (isset($_REQUEST['action'])){
				$action = $_REQUEST['action'];
			}
			else {
				$action = NULL ;
			} 

			switch($action) {
	
				//pas d'action, on réinitialise 1er appel
				case NULL:
					$lesNews = $gateWayNews->affichageNewsPageHome(0,10);
					require($vues['home']);
					break;
	
				case "login":				
					$this->login();
				
				case "signUp":
					$this->signUp();
								
				case "myNews":
					$this->myNews();

				case "addNews":
					$this->addNews();	

				case "deleteNews":
					deleteNews();
					break;
	
				//mauvaise action
				default:
					$dVueEreur[] = "Erreur 404 !!";
					require($rep.$vues['erreur']);
					break;


			}
		} 
		catch (Exception $e)
		{
			$dVueEreur[] =	"Erreur inattendue!!! ";
			require ($rep.$vues['erreur']);
		}
	
		exit(0);
		}//fin constructeur

		function login(){
			if ( !isset($_POST['submit'])){
				require($rep.$vues['home']);
				require($rep.$vues['login']);
			}
			else {
				//Valider la saisies
				//Valider l'utilisateur
				//Donner un cookie
				//redirection sur mes news
				require($rep.$vues['login']);
			}
		}
		
		function signUp(){
			if ( !isset($_POST['submit'])){
				require($rep.$vues['home']);
				require($rep.$vues['signUp']);
			}
			else {
				//Valider la saisies
				//Valider l'utilisateur
				//redirection sur login
				require($rep.$vues['signUp']);
			}
		}
		
		function myNews(){
			$lesNews = $gateWayNews->affichageNewsPageMesNews();
			require($rep.$vues['myNews']);
		}
		
		function addNews(){
			if ( 1 == 1/*Vérifier si connecter  */){
				//Verifier si il existe 
				//Veridier la saisie
				//add news
			}
			else {
				require($rep.$vues['login']);
			}
		}
		
}//fin class

?>
