<?php

class gateWayNews {

    private $con ;

    function __construct($con) {
        require_once("modeles/News.php");
        $this->con = $con;
    }
    /*
    private function returnTabNews($data){       
        Foreach ($data as $row){  
            $tab[] = new News($row['date'],$row['titre'],$row['contenu']);
        }
        return $tab;  
    }
    */

    public function affichageNewsPageHome($start,$limit){
        
        $query = "SELECT * FROM News ORDER BY date LIMIT :start,:limit" ;          
        $this->con->executeQuery($query,array(':start' => array($start, PDO::PARAM_INT),
                                        ':limit' => array($limit, PDO::PARAM_INT)));
        return $this->con->getResults();
    }

    public function affichageNewsPageMesNews($user): array{
        $query = "SELECT * FROM News WHERE XXXXX=user=:user";
        $con->executeQuery($query,array(':user' => array($user,PDO::PARAM_INT)));
        return returnTabNews($this->con->getResults());
    }

    public function modifierNews($key,$newContenu,$newTitre){
        $query = "UPDATE News SET contenu=:newContenu and titre=:newTitre WHERE user=:key";
        $con->executeQuery($query,array(':newContenu' => array($newContenu,PDO::PARAM_STR),
                                        ':newTitre' => array($newTitre,PDO::PARAM_STR),
                                        ':user' => array($key,PDO::PARAM_INT)));
    }

    public function supprimerNews(){
        $query = "DELETE News WHERE xxxxxxxxxxxx";
        $con->executeQuery($query,array());
    }


    public function creerNews(){
        $query = "INSERT INTO News(xx,xx,xx,xx) VALUES ";
        $con->executeQuery($query,array());
    }

    /*
    public function printAllNews(){
        $query = "SELECT * FROM News";
        $this->con->executeQuery($query);
        return $this->con->getResults();
    }


    public function insertNews($news){
        if ( strlen($news->titre) == 0 or  strlen($news->contenu) == 0){
            throw new Exception('Class -> NewsGateway /
                                Fonction -> insertNews /
                                Titre ou contenu est vide !!');
        }
        $query = "INSERT INTO News (titre,contenu) VALUES (:titre,:contenu)";
        $this->con->executeQuery($query,array(':titre' => array($news->titre, PDO::PARAM_STR),
                                            ':contenu' => array($news->contenu, PDO::PARAM_STR)));
    }
    */
}
?>